package it.uniroma3.siw.museo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.museo.model.Collezione;
import it.uniroma3.siw.museo.service.CollezioneService;
import it.uniroma3.siw.museo.service.DipendenteService;
import it.uniroma3.siw.museo.validator.CollezioneValidator;

@Controller
public class CollezioneController {
	@Autowired
	private CollezioneService collezioneService;

	@Autowired
	private DipendenteService dipendenteService;
	
	@Autowired
	private CollezioneValidator collezioneValidator;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping(value="/collezione", method=RequestMethod.GET)
	public String getCollezioneTest() {
		return "collezione.html";
	}
	
	@RequestMapping(value="/collezione/{id}", method=RequestMethod.GET)
	public String getCollezione(Model model, @PathVariable("id") Long id) {
		model.addAttribute("collezione", collezioneService.findById(id));
		return "collezione.html";
	}
	
	@RequestMapping(value="/collezioni", method=RequestMethod.GET)
	public String getAllCollezioni(Model model) {
		model.addAttribute("collezioni", collezioneService.findAll());
		return "collezioni.html";
	}
	
	@RequestMapping(value="/private/addCollezione", method=RequestMethod.GET)
	public String addCollezione(Model model) {
		model.addAttribute("dipendenti", dipendenteService.findAll());
		model.addAttribute("collezione", new Collezione());
		return "collezioneForm.html";
	}
	
	@RequestMapping(value="/collezione", method=RequestMethod.POST)
	public String newCollezione(Model model, @ModelAttribute("collezione") Collezione collezione, BindingResult bindingResult) {
		this.collezioneValidator.validate(collezione, bindingResult);
		logger.debug("confermaCollezione");
		if (!bindingResult.hasErrors()) {
			collezioneService.saveOrUpdate(collezione);
			model.addAttribute("collezioni", collezioneService.findAll());
			return "collezioni.html";
		}
		model.addAttribute("dipendenti", dipendenteService.findAll());
		model.addAttribute("collezione", collezione);
		return "collezioneForm.html";
	}
	

	@RequestMapping(value="/collezione/elimina", method=RequestMethod.POST)
	public String eliminaCollezione(Model model, @ModelAttribute("id") String id) {
		if(collezioneService.findById(Long.parseLong(id)) != null) {
			collezioneService.deleteById(Long.parseLong(id));
		}
		model.addAttribute("collezioni", collezioneService.findAll());
		return "collezioni.html";
	}
		
}
