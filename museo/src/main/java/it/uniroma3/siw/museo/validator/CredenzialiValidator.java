package it.uniroma3.siw.museo.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.siw.museo.model.Credenziali;
import it.uniroma3.siw.museo.service.CredenzialiService;

@Component
public class CredenzialiValidator implements Validator{
	
	@Autowired
	CredenzialiService credenzialiService;
	
	final Integer MIN_PASSWORD_LENGTH = 6;
	final Integer MAX_PASSWORD_LENGTH = 20;
	final Integer MIN_USERNAME_LENGTH = 4;
	final Integer MAX_USERNAME_LENGTH = 20;
	
    private static final Logger logger = LoggerFactory.getLogger(CredenzialiValidator.class);

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "required");

		Credenziali c = (Credenziali) o;
		String username = c.getUsername().trim();
		String password = c.getPassword().trim();
		
		if(credenzialiService.getCredenziali(username) != null)
			errors.rejectValue("username", "duplicato");
		
		if(password.length() < MIN_PASSWORD_LENGTH || password.length() > MAX_PASSWORD_LENGTH)
			errors.rejectValue("password", "dimensione");
		
		if(username.length() < MIN_USERNAME_LENGTH || username.length() > MAX_USERNAME_LENGTH)
			errors.rejectValue("username", "dimensione");
		
		if (!errors.hasErrors()) {
			logger.debug("confermato: valori non nulli");
		}
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return Credenziali.class.equals(aClass);
	}
}

