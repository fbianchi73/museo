package it.uniroma3.siw.museo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.museo.model.Dipendente;
import it.uniroma3.siw.museo.repository.DipendenteRepository;

@Service
public class DipendenteService {
	@Autowired
	private DipendenteRepository dipendenteRepository;
	
	@Transactional
	public Dipendente findById(Long id) {
		return dipendenteRepository.findById(id).get();
	}
	
	@Transactional
	public List<Dipendente> findAll() {
		return (List<Dipendente>) dipendenteRepository.findAll();
	}
}