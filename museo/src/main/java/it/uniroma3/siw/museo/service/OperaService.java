package it.uniroma3.siw.museo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.museo.model.Opera;
import it.uniroma3.siw.museo.repository.OperaRepository;

@Service
public class OperaService {
	@Autowired
	private OperaRepository operaRepository;
	
	@Transactional
	public Opera findById(Long id) {
		return operaRepository.findById(id).get();
	}
	
	@Transactional
	public List<Opera> findAllByCollezioneId(Long id) {
		return (List<Opera>) operaRepository.findAllByCollezioneId(id);
	}
	
	@Transactional
	public void deleteById(Long id) {
		operaRepository.deleteById(id);
	}
	
	@Transactional
	public void saveOrUpdate(Opera opera) {
		operaRepository.save(opera);
	}
	
	@Transactional
	public List<Opera> findAll() {
		return (List<Opera>) operaRepository.findAll();
	}
	
	@Transactional
	public List<Opera> findFirst4() {
		return (List<Opera>) operaRepository.findTop4ByIdIsNotNull();
	}
}
