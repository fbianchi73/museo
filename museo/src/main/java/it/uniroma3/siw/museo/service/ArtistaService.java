package it.uniroma3.siw.museo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.museo.model.Artista;
import it.uniroma3.siw.museo.repository.ArtistaRepository;

@Service
public class ArtistaService {
	@Autowired
	private ArtistaRepository artistaRepository;
	
	@Transactional
	public Artista findById(Long id) {
		return artistaRepository.findById(id).get();
	}
	
	@Transactional
	public List<Artista> findAll() {
		return (List<Artista>) artistaRepository.findAll();
	}
	
	@Transactional
	public void deleteById(Long id) {
		artistaRepository.deleteById(id);
	}
	
	@Transactional
	public void saveOrUpdate(Artista artista) {
		artistaRepository.save(artista);
	}
}
