package it.uniroma3.siw.museo.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import it.uniroma3.siw.museo.model.Artista;
import it.uniroma3.siw.museo.model.Collezione;
import it.uniroma3.siw.museo.model.Opera;
import it.uniroma3.siw.museo.service.ArtistaService;
import it.uniroma3.siw.museo.service.CollezioneService;
import it.uniroma3.siw.museo.service.FileService;
import it.uniroma3.siw.museo.service.OperaService;
import it.uniroma3.siw.museo.validator.OperaValidator;

@Controller
public class OperaController {
	@Autowired
	private OperaService operaService;
	
	@Autowired
	private ArtistaService artistaService;
	
	@Autowired
	private CollezioneService collezioneService;
	
	@Autowired
	private FileService fileService;

	@Autowired
	private OperaValidator operaValidator;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value="/",  method=RequestMethod.GET)
	public String home(Model model, HttpSession session){
		model.addAttribute("opere", operaService.findFirst4());
		return "index.html";
	}
	
	@RequestMapping(value="/index",  method=RequestMethod.GET)
	public String index(Model model, HttpSession session){
		model.addAttribute("opere", operaService.findFirst4());
		return "index.html";
	}
	
	@RequestMapping(value="/opera", method=RequestMethod.GET)
	public String getOperaTest() {
		return "opera.html";
	}
	
	@RequestMapping(value="/opera/{id}", method=RequestMethod.GET)
	public String getOpera(Model model, @PathVariable("id") Long id) {
		model.addAttribute("opera", operaService.findById(id));
		return "opera.html";
	}
	
	@RequestMapping(value="/private/addOpera", method=RequestMethod.GET)
	public String addOpera(Model model) {
		model.addAttribute("opera", new Opera());
		model.addAttribute("artisti", artistaService.findAll());
		model.addAttribute("collezioni", collezioneService.findAll());
		return "operaForm.html";
	}

	@RequestMapping(value="/opera/inserisci", method=RequestMethod.POST)
	public String inserisciOpera(Model model, @ModelAttribute("opera") Opera opera, BindingResult bindingResult, @RequestParam("file") MultipartFile file) {
		this.operaValidator.validate(opera, bindingResult);
		logger.debug("confermaAttivita");
		if (!bindingResult.hasErrors()) {
			opera.setImmagine(fileService.upload(file));
			model.addAttribute("opera", opera);
			return "operaConferma.html";
		}
		model.addAttribute("artisti", artistaService.findAll());
		model.addAttribute("collezioni", collezioneService.findAll());
		model.addAttribute("opera", opera);
		return "operaForm.html";
	}
	
	@RequestMapping(value="/opera", method=RequestMethod.POST)
	public String salvaOpera(Model model, @ModelAttribute("opera") Opera opera, BindingResult bindingResult) {
		this.operaValidator.validate(opera, bindingResult);
		logger.debug("confermaAttivita");
		operaService.saveOrUpdate(opera);
		model.addAttribute("artisti", artistaService.findAll());
		return "artisti.html";
	}
	
	@RequestMapping(value="/opera/elimina", method=RequestMethod.POST)
	public String eliminaOpera(Model model, @ModelAttribute("id") String id, @ModelAttribute("callback") String callback) {
		Collezione collezione;
		Artista artista;
		Opera opera = operaService.findById(Long.parseLong(id));
		if(opera != null) {
			operaService.deleteById(Long.parseLong(id));
		}
		if(callback.equals("collezione")) {
			collezione = opera.getCollezione();	
			model.addAttribute("collezione", collezione);
			return "collezione.html";
		}
		if(callback.equals("artista")) {
			artista = opera.getArtista();	
			model.addAttribute("artista", artista);
			return "artista.html";
		}
		return "index.html";
	}
}
