package it.uniroma3.siw.museo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.siw.museo.model.Collezione;
import it.uniroma3.siw.museo.repository.CollezioneRepository;

@Service
public class CollezioneService {
	@Autowired
	private CollezioneRepository collezioneRepository;
	
	@Transactional
	public Collezione findById(Long id) {
		return collezioneRepository.findById(id).get();
	}
	
	@Transactional
	public List<Collezione> findAll() {
		return (List<Collezione>) collezioneRepository.findAll();
	}
	
	@Transactional
	public void deleteById(Long id) {
		collezioneRepository.deleteById(id);
	}
	
	@Transactional
	public void saveOrUpdate(Collezione collezione) {
		collezioneRepository.save(collezione);
	}

}
