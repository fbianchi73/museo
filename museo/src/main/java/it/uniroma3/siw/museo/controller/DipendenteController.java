package it.uniroma3.siw.museo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.museo.model.Credenziali;
import it.uniroma3.siw.museo.service.CredenzialiService;

@Controller
public class DipendenteController {
	@Autowired
	private CredenzialiService credenzialiService;
	
	@RequestMapping(value="/utenti", method=RequestMethod.GET)
	public String getUtenti(Model model) {
		model.addAttribute("utenti", credenzialiService.findAllNotAdmin());
		return "utenti.html";
	}
	
	@RequestMapping(value="/utenti", method=RequestMethod.POST)
	public String setAdmin(Model model, @ModelAttribute("username") String username) {
		Credenziali c = credenzialiService.getCredenziali(username);
		if(c != null) {
			credenzialiService.saveCredenziali(c, Credenziali.ADMIN_ROLE);
		}
		model.addAttribute("utenti", credenzialiService.findAllNotAdmin());
		return "utenti.html";
	}
}
