package it.uniroma3.siw.museo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.uniroma3.siw.museo.model.Opera;

@Repository
public interface OperaRepository extends CrudRepository<Opera, Long>{
	public List<Opera> findAllByCollezioneId(Long id);
	public List<Opera> findTop4ByIdIsNotNull();
}
