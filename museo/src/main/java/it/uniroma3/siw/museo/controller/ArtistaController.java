package it.uniroma3.siw.museo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.museo.model.Artista;
import it.uniroma3.siw.museo.service.ArtistaService;
import it.uniroma3.siw.museo.validator.ArtistaValidator;

@Controller
public class ArtistaController {
	@Autowired
	private ArtistaService artistaService;
	
	@Autowired
	private ArtistaValidator artistaValidator;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping(value="/artista", method=RequestMethod.GET)
	public String getArtistaTest() {
		return "artista.html";
	}
	
	@RequestMapping(value="/artista/{id}", method=RequestMethod.GET)
	public String getArtista(Model model, @PathVariable("id") Long id) {
		model.addAttribute("artista", artistaService.findById(id));
		return "artista.html";
	}
	
	@RequestMapping(value="/artisti", method=RequestMethod.GET)
	public String getAllArtisti(Model model) {
		model.addAttribute("artisti", artistaService.findAll());
		return "artisti.html";
	}
	
	@RequestMapping(value="/private/addArtista", method=RequestMethod.GET)
	public String addCollezione(Model model) {
		model.addAttribute("artista", new Artista());
		return "artistaForm.html";
	}

	@RequestMapping(value="/artista", method=RequestMethod.POST)
	public String newOpera(Model model, @ModelAttribute("artista") Artista artista, BindingResult bindingResult) {
		this.artistaValidator.validate(artista, bindingResult);
		logger.debug("confermaArtista");
		if (!bindingResult.hasErrors()) {
			artistaService.saveOrUpdate(artista);
			model.addAttribute("artisti", artistaService.findAll());
			return "artisti.html";
		}
		model.addAttribute("artista", artista);
		return "artistaForm.html";
	}
	
	@RequestMapping(value="/artista/elimina", method=RequestMethod.POST)
	public String eliminaCollezione(Model model, @ModelAttribute("id") String id) {
		if(artistaService.findById(Long.parseLong(id)) != null) {
			artistaService.deleteById(Long.parseLong(id));
		}
		model.addAttribute("artisti", artistaService.findAll());
		return "artisti.html";
	}
}
