package it.uniroma3.siw.museo.session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import it.uniroma3.siw.museo.model.Credenziali;
import it.uniroma3.siw.museo.repository.CredenzialiRepository;

@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class DatiSessione {
	private Credenziali credenziali;
	
	@Autowired
	private CredenzialiRepository credenzialiRepository;
	
	private void update() {
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UserDetails loggedUserDetails = (UserDetails) obj;
		
		credenziali = credenzialiRepository.findByUsername(loggedUserDetails.getUsername()).get();
		credenziali.setPassword("[]");
	}
	
	public boolean isAdmin() {
		if(credenziali == null)
			update();
		return credenziali.getRole().equals(Credenziali.ADMIN_ROLE);
	}

}
