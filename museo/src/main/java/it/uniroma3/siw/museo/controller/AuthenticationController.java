package it.uniroma3.siw.museo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.museo.model.Credenziali;
import it.uniroma3.siw.museo.model.Dipendente;
import it.uniroma3.siw.museo.service.CredenzialiService;
import it.uniroma3.siw.museo.validator.CredenzialiValidator;
import it.uniroma3.siw.museo.validator.DipendenteValidator;

@Controller
public class AuthenticationController {
	@Autowired
	CredenzialiService credenzialiService;
	
	@Autowired
	DipendenteValidator dipendenteValidator;
	
	@Autowired
	CredenzialiValidator credenzialiValidator;
	
	@RequestMapping(value="/signup", method=RequestMethod.GET)
	public String showFormRegistrazione(Model model) {
		model.addAttribute("dipendente", new Dipendente());
		model.addAttribute("credenziali", new Credenziali());
		
		return "registrazioneForm.html";
	}
	
	@RequestMapping(value="/signup", method=RequestMethod.POST)
	public String registraUtente(@ModelAttribute("dipendente") Dipendente dipendente, BindingResult dipendenteBindingResult,
								 @ModelAttribute("credenziali") Credenziali credenziali, BindingResult credenzialiBindingResult,
								 Model model){
		this.dipendenteValidator.validate(dipendente, dipendenteBindingResult);
		this.credenzialiValidator.validate(credenziali, credenzialiBindingResult);
		if(!dipendenteBindingResult.hasErrors() && !credenzialiBindingResult.hasErrors()) {
			credenziali.setDipendente(dipendente);
			credenzialiService.saveCredenziali(credenziali);
			return "login.html";
		}
		return "registrazioneForm.html";
	}
}
